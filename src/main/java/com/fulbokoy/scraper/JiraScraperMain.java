package com.fulbokoy.scraper;

import com.atlassian.jira.rest.client.api.domain.Issue;
import com.atlassian.jira.rest.client.api.domain.IssueField;
import com.atlassian.jira.rest.client.api.domain.Project;
import com.fulbokoy.core.ScraperJRJC;
import com.fulbokoy.core.Template;
import com.fulbokoy.core.LatexTemplate;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by alonso on 13-05-17.
 */
public class JiraScraperMain {



    public static void main(String args[]){
        Logger logger = Logger.getLogger("logger");

        ScraperJRJC scraper = new ScraperJRJC();
        Project prj = scraper.getProject("VIT");

        List<Issue> issues = scraper.getIssueList(prj);
        LatexTemplate latexTemplate = LatexTemplate.getInstance();

        logger.info(issues.size() + "");
        //Issue issue = issues.get(issues.size() - 1);
        
        for (Issue issue: issues){
            logger.info("Processing issue " + issue.getKey());
            IssueField issueField = issue.getField("customfield_10200");
            String desc = issue.getDescription();
            if (desc != null){
                desc = latexTemplate.createDescription(desc);
            } else {
                desc = "";
            }

            latexTemplate.replaceInTemplate(Template.ISSUE_NUMBER, issue.getKey());
            latexTemplate.replaceInTemplate(Template.ISSUE_TYPE, issue.getIssueType().getName());
            latexTemplate.replaceInTemplate(Template.ISSUE_NAME, issue.getSummary());
            latexTemplate.replaceInTemplate(Template.ISSUE_SUMMARY, desc);
            latexTemplate.replaceInTemplate(Template.ISSUE_EPIC, issueField.getValue() != null ?
                    issueField.getValue().toString():"");
            latexTemplate.commitTemplate();
        }

        //logger.info(issue.toString());//ISSUE_SUMMARY
        //logger.info(latexTemplate.getTemplateInstance());
        try {
            Files.write(Paths.get("./template.txt"), latexTemplate.getDocument().getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
