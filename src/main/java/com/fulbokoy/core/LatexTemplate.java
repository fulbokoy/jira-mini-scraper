package com.fulbokoy.core;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by alonso on 5/16/17.
 */
public class LatexTemplate extends Template{
    private static LatexTemplate ourInstance = new LatexTemplate();
    
    private String template = "\\subsection*{{\\small NUMBER}\\\\ Issue NAME}\n" +
            "\\textbf{Type: }TYPE\\\\\n" +
            "\\textbf{Epic Name: }{\\color{blue}EPICLINK}\n" +
            "\\subsubsection*{Description \\xrfill[.5ex]{.4pt}[lightgray]}\n" +
            "\\begin{adjustwidth}{0.5cm}{}\n" +
            "SUMMARY\n" +
            "\\end{adjustwidth}\n" +
            "\\subsubsection*{Attachments \\xrfill[.5ex]{.4pt}[lightgray]}\n\n";

    private String itemizeTemplate = "\\begin{itemize}\n" +
            "CONTENT\n" +
            "\\end{itemize}";

    private String enumerateTemplate = "\\begin{enumerate}\n" +
            "CONTENT\n" +
            "\\end{enumerate}";

    private String templateInstance = template;

    private StringBuffer document = new StringBuffer();

    private String[] specialCharacters = new String[]{"&", "_", "$"};

    public static LatexTemplate getInstance() {
        return ourInstance;
    }

    private LatexTemplate() {
    }

    @Override
    public String getTemplateString() {
        return template;
    }

    @Override
    public void replaceInTemplate(String placeholder, String value) {
        String replacedValue = escapeSpecialChars(value);
        templateInstance = templateInstance.replace(placeholder, replacedValue);
    }

    public String createDescription(String desc){
        //Replace *text* -> \textbf{text}
        String description = desc.replaceAll("\\{color:(\\w+)\\}(.*?)\\{color\\}","{\\\\color{$1}$2}");
        description = description.replaceAll("\\*([\\p{L}\\s&&[^#]&&[^\\n]]+)\\*+?", "\\\\textbf{$1}");
        description = createDescriptionList(description.split("\n"), 0, 0,
                "CONTENT0", enumerateTemplate, itemizeTemplate);

        return description;
    }

    private String createDescriptionList(String[] lines, int index, int currentLevel, String description,
                                         String enumTemplate, String itemTemplate){
        if (index >= lines.length){
            return description.replaceAll("CONTENT\\d", "");
        }//end case

        String line = lines[index].trim();
        Pattern pattern = Pattern.compile("^\\s*([*#]{1,})\\s(.*)");
        Matcher matcher = pattern.matcher(line);

        if (matcher.find()){
            String sublistTemplate = "";
            String levelString = matcher.group(1);
            int level = levelString.length();

            if (currentLevel == 0 || currentLevel < level){
                if (levelString.matches("#$")){
                    sublistTemplate = enumTemplate;
                } else {
                    sublistTemplate = itemTemplate;
                }

                sublistTemplate = sublistTemplate.replace("CONTENT", "\\item " + matcher.group(2) +
                        "\nCONTENT" + level);
                description = description.replace("CONTENT" + currentLevel, sublistTemplate + " \n" +
                        "\nCONTENT" + currentLevel);
            } else if (currentLevel == level){
                description = description.replace("CONTENT" + currentLevel, "\\item " +
                        matcher.group(2) + "\nCONTENT" + currentLevel);
            } else if (currentLevel > level){
                description = description.replace("CONTENT" + level, "\\item " +
                        matcher.group(2) + "\nCONTENT" + level);
                //clean up already closed levels
                for (int i = level + 1; i <= currentLevel;i++){
                    description = description.replace("CONTENT" + i, "");
                }
            }

            currentLevel = level;
        } else {//nothing to match, this means a simple line or a table :o
            pattern = Pattern.compile("\\s*(\\|+?|\\|\\|+?)");
            matcher = pattern.matcher(line);

            if (matcher.find()) {//it's a table!

            } else {
                description = description.replaceAll("CONTENT\\d", "") + line + "\\par\nCONTENT0";
                currentLevel = 0;
            }
        }

        return createDescriptionList(lines, index+1, currentLevel, description, enumTemplate, itemTemplate);
    }

    @Override
    public String getTemplateInstance() {
        return templateInstance;
    }

    public String getDocument(){
        return document.toString();
    }

    public void appendToDocument(String value){
        this.document.append(value);
    }

    public void commitTemplate(){
        document.append(templateInstance);
        templateInstance = template;
    }

    private String escapeSpecialChars(String value){
        String replacedValue = value;
        for (String specialChar : specialCharacters){
            replacedValue = replacedValue.replace(specialChar, "\\"+specialChar);
        }

        return replacedValue;
    }

    public String createTable(String[] content, int from){

        return "";
    }
}