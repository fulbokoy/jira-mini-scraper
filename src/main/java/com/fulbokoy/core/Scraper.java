package com.fulbokoy.core;

import com.lesstif.jira.project.Project;
import com.lesstif.jira.services.ProjectService;
import org.apache.commons.configuration.ConfigurationException;

import java.io.IOException;

/**
 * Created by alonso on 13-05-17.
 */
public class Scraper {

    public Project getProject(String projectKey) {
        ProjectService prjService = null;
        Project prj = null;
        
        try {
            prjService = new ProjectService();
            prj = prjService.getProjectDetail(projectKey);
        } catch (ConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        return prj;
    }
}