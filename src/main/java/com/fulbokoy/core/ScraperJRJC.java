package com.fulbokoy.core;

import com.atlassian.jira.rest.client.api.JiraRestClient;
import com.atlassian.jira.rest.client.api.ProjectRestClient;
import com.atlassian.jira.rest.client.api.domain.BasicComponent;
import com.atlassian.jira.rest.client.api.domain.Issue;
import com.atlassian.jira.rest.client.api.domain.Project;
import com.atlassian.jira.rest.client.api.domain.SearchResult;
import com.atlassian.jira.rest.client.internal.async.AsynchronousJiraRestClientFactory;
import org.apache.commons.configuration.ConfigurationException;

import java.io.IOException;
import java.util.*;

import static java.util.Collections.singletonList;

/**
 * Created by alonso on 13-05-17.
 */
public class ScraperJRJC {

    final AsynchronousJiraRestClientFactory factory = new AsynchronousJiraRestClientFactory();
    private JiraRestClient restClient;

    public ScraperJRJC() {

        try {
            Config config = new Config();
            restClient = factory.createWithBasicHttpAuthentication(config.getUrl(),
                    config.getUsername(), config.getPassword());
        } catch (ConfigurationException e) {
            e.printStackTrace();
        }
    }

    public Project getProject(String key){
        ProjectRestClient projectRestClient = restClient.getProjectClient();
        Project project = (Project) projectRestClient.getProject(key).claim();

        return project;
    }

    public List<Issue> getIssueList(Project project){
        SearchResult searchResult = restClient.getSearchClient().searchJql("project=\"" +
                project.getKey() + "\" ORDER BY key ASC, reporter ASC, created DESC", 1000, 0, new HashSet<>()).claim();
        
        Iterable<Issue> issuesResult = searchResult.getIssues();
        List<Issue> issues = new ArrayList<>();

        issuesResult.forEach(issues::add);

        try {
            restClient.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return issues;
    }
}
