package com.fulbokoy.core;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

import java.io.File;
import java.net.URI;

/**
 * Created by alonso on 13-05-17.
 */
public class Config {
    private String username;
    private String password;
    private URI url;


    public Config() throws ConfigurationException {
        File configFile = null;
        PropertiesConfiguration config = null;

        if(System.getProperty("jira.client.property") != null) {
            configFile = new File(System.getProperty("jira.client.property"));
        }

        if(configFile != null && configFile.exists()) {
            config = new PropertiesConfiguration(configFile);
        } else {
            configFile = new File(new File("."), "jira-rest-client.properties");
            if(configFile.exists()) {
                config = new PropertiesConfiguration(configFile);
            } else {
                config = new PropertiesConfiguration("jira-rest-client.properties");
            }
        }

        username = config.getString("jira.user.id");
        password = config.getString("jira.user.pwd");
        url = URI.create(config.getString("jira.server.url"));
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public URI getUrl() {
        return url;
    }
}
