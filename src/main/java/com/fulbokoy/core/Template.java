package com.fulbokoy.core;

/**
 * Created by alonso on 5/16/17.
 */
public abstract class Template {
    public static final String ISSUE_NUMBER = "NUMBER";
    public static final String ISSUE_NAME = "NAME";
    public static final String ISSUE_TYPE = "TYPE";
    public static final String ISSUE_EPIC = "EPICLINK";
    public static final String ISSUE_SUMMARY = "SUMMARY";
    public static final String ISSUE_ACCEPTANCE = "ISSUE_ACCEPTANCE";
    protected String HEADER_PATTERN = "^h([0 - 6])\\.(. *)$";
    protected String EMP_PATTERN = "([*_])(.*)";

    public abstract String getTemplateString();
    public abstract void replaceInTemplate(String placeholder, String value);
    public abstract String getTemplateInstance();

    public String jiraMDToMD(String input) {
        //input = input.replaceAll("\\*([\\w\\s\\n\\r]+)\\*", "*$1*");
        input = input.replaceAll("\\{\\{([^}]+)\\}\\}", "`$1`");
        input = input.replaceAll("\\?\\?((?:.[^?]|[^?].)+)\\?\\?", "<cite>$1</cite>");
        input = input.replaceAll("\\+([^+]*)\\+", "<ins>$1</ins>");
        input = input.replaceAll("\\^([^^]*)\\^", "<sup>$1</sup>");
        input = input.replaceAll("~([^~]*)~", "<sub>$1</sub>");
        input = input.replaceAll("-([^-]*)-", "-$1-");
        input = input.replaceAll("\\{code(:([a-z]+))?\\}([^]*)\\{code\\}", "```$2$3```");

        return input;
    }
}
